<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2004 The zen-cart developers                           |
// |                                                                      |
// | http://www.zen-cart.com/index.php                                    |
// |                                                                      |
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
// $Id:recaptcha_functions.php,v1.1 2010/02/03 pickupman $
//
if( !class_exists('messageStack') )
{
	include('../../classes/message_stack.php');
}
if( !is_object($reloadedStack) )
{
	$reloadedStack = new messageStack();
}


if($_GET['remove_recaptcha'] == 'yes' AND basename($_SERVER['PHP_SELF']) == 'index.php')
{
	remove_recaptcha();
}
if($_GET['install_recaptcha'] == 'yes' AND basename($_SERVER['PHP_SELF']) == 'index.php')
{
	install_recaptcha();	
}

    // check installation of recaptcha
    $captchaCheck = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'reCAPTCHA' LIMIT 1");
    if($captchaCheck->RecordCount() < 1 AND basename($_SERVER['PHP_SELF']) == 'index.php')
    {        
        $reloadedStack->add('reCAPTCHA is not currently installed. <a href="index.php?install_recaptcha=yes">[Install Now]</a>');
    }
    if($captchaCheck->RecordCount() > 0 AND basename($_SERVER['PHP_SELF']) == FILENAME_CONFIGURATION . '.php' AND $captchaCheck->fields['configuration_group_id'] == $_GET['gID']){
        $reloadedStack->add('reCAPTCHA is installed. <a href="index.php?remove_recaptcha=yes">[Click to remove/upgrade]</a>', 'success');
		
    }
	

function install_recaptcha() {
	global $db, $reloadedStack;
	$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " VALUES ('', 'reCAPTCHA', 'Config options for reCAPTCHA text', '1', '1')");
	
	$group_id = (function_exists('zen_db_insert_id')) ? zen_db_insert_id() : $db->insert_ID();
	
	$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $group_id . " WHERE configuration_group_id = " . $group_id);
	$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " VALUES 
		('', 'reCAPTCHA Public Key', 'CONTACT_US_RECAPTCHA_PUBLIC_KEY', '', 'Public key given from reCAPTCHA website (default: blank).', " . $group_id . ", '0', NULL, now(), NULL, NULL),
		('', 'reCAPTCHA Private Key', 'CONTACT_US_RECAPTCHA_PRIVATE_KEY', '', 'Private key given from reCAPTCHA website (default: blank).', " . $group_id . ", '0', NULL, now(), NULL, NULL),
		('', 'reCAPTCHA Theme', 'CONTACT_US_RECAPTCHA_THEME', 'white', 'Choose a theme option for the widget.', " . $group_id . ", '1', NULL, now(), NULL, 'zen_cfg_select_option(array(\"red\", \"white\", \"blackglass\", \"clean\"),'),
        ('', 'Enable Contact Form', 'CONTACT_US_RECAPTCHA_STATUS', 'true', 'Disply reCAPTCHA text on contact form (default: true)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
        ('', 'Enable Advanced Search Form', 'ADVANCED_SEARCH_RECAPTCHA_STATUS', 'false', 'Disply reCAPTCHA text on advanced search form (default: false)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
		('', 'Enable Tell A Friend Form', 'TELL_A_FRIEND_RECAPTCHA_STATUS', 'false', 'Disply reCAPTCHA text on tell a friend form (default: false)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
        ('', 'Enable Create Account Form', 'CREATE_ACCOUNT_RECAPTCHA_STATUS', 'false', 'Disply reCAPTCHA text on create account (default: false)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
        ('', 'Enable Login Form', 'LOGIN_RECAPTCHA_STATUS', 'false', 'Disply reCAPTCHA text on login form (default: false)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
        ('', 'Enable Product Write Review Form', 'PRODUCT_REVIEWS_WRITE_RECAPTCHA_STATUS', 'false', 'Disply reCAPTCHA text on product write review form (default: false)', " . $group_id . ", '9', NULL, now(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),')
        		
		");
		
	
	//Check admin menu registration
	if(substr(PROJECT_VERSION_MINOR, 0, 1) == '5')
	{
		$recaptchaCheck = $db->Execute("SELECT page_key FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configRecaptcha'");
        if ($recaptchaCheck->RecordCount() < 1)
        {
            $db->Execute("INSERT INTO " . TABLE_ADMIN_PAGES . " (page_key, language_key, main_page, menu_key, display_on_menu, sort_order, page_params) VALUES ('configRecaptcha', 'BOX_TOOLS_RECAPTCHA', 'FILENAME_CONFIGURATION', 'configuration', 'Y', '" . $group_id ."','gID=" . $group_id . "')");
        }
	}
    $reloadedStack->add('reCAPTCHA successfully installed. <a href="configuration.php?gID=' . $group_id . '">[Click to configure]</a>','success');
}

/**
 * reCaptcha Keys
 * @param none
 * @return mixed array configuration keys
 */
function recaptcha_keys(){
    // define array of configuration keys
    return array('CONTACT_US_RECAPTCHA_STATUS','ADVANCED_SEARCH_RECAPTCHA_STATUS','TELL_A_FRIEND_RECAPTCHA_STATUS', 'CREATE_ACCOUNT_RECAPTCHA_STATUS', 'LOGIN_RECAPTCHA_STATUS', 'PRODUCT_REVIEWS_WRITE_RECAPTCHA_STATUS', 'CONTACT_US_RECAPTCHA_PUBLIC_KEY','CONTACT_US_RECAPTCHA_PRIVATE_KEY','CONTACT_US_RECAPTCHA_THEME');
} 

function remove_recaptcha() {
	global $db, $reloadedStack;
	
	$db->Execute("delete from " . TABLE_CONFIGURATION_GROUP . " where configuration_group_title = 'reCAPTCHA'");
	
	if(PROJECT_VERSION_MINOR == '5.0')
	{
		$db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configRecaptcha'");
	}  
	
    $keys = recaptcha_keys();	
	$db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $keys) . "')");
    $reloadedStack->add('reCAPTCHA successfully removed!','success');
}

/* End of file */