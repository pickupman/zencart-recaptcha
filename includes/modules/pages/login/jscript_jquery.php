<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js?legacy"></script>

<script type="text/javascript" language="javascript">

    function showRecaptcha(element, submitButton, recaptchaButton) {
	  Recaptcha.destroy();
	  Recaptcha.create('<?php echo CONTACT_US_RECAPTCHA_PUBLIC_KEY;?>', element, {
			theme: '<?php echo CONTACT_US_RECAPTCHA_THEME;?>',
			tabindex: 0,
			callback: Recaptcha.focus_response_field
	  });
	  $(".submit").hide();
	  $(".recaptcha_required").show();
	  $("#"+recaptchaButton).hide();
	  $("#"+submitButton).show();
  
    }
    jQuery(document).ready(function($){
        <?php if(LOGIN_RECAPTCHA_STATUS == 'true' && CREATE_ACCOUNT_RECAPTCHA_STATUS == 'true') : ?>
        $(".submit").hide();
        
        $("#recaptcha_required_1").click(function(){
            showRecaptcha('recaptcha_div_1', 'submit_1', $(this).attr('id'));
        });
        
        $("#recaptcha_required_2").click(function(){
            showRecaptcha('recaptcha_div_2', 'submit_2', $(this).attr('id'));
        });
        <?php endif;?>
    });
</script>