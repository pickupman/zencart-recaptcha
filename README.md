------------------------
reCaptcha Security Check
------------------------

This was adapted from osCommerce. This contribution was made originally for osC by

Author: Pietro Licata <p.licata@webmad.it>

This contribution has been rewritten to work for zen cart. This contribution uses
the override system so only 1 core file is touched. The sql statements will autoload.
Adapted for zencart by Joe McFrederick 2009/06/23

reCaptcha is a publicly available captcha with a cool feature: it helps to digitize book.
This small modification will help you to add a reCaptcha form on your Contact Us form, in order to protect you from abuses of Contact Us form.
I based my modification on OSC v2.2 RC1. Adapted to zen cart 1.3.8 - 1.5


---------------
Version History
---------------


v.1.3.3
=======
Added support for admin menus in 1.5.x versions


v.1.3.2
=======
Added custom messageStack loader for installation messages


v.1.3.1
=======
Few more syntax updates for 1.5 upgrade
Remove tell-a-friend page as it has been removed from the 1.5 release


v.1.3
=====
Update code for 1.5 version of Zencart
Fixed installation routine to version check


v.1.2.6
=======
Fix recaptcha when create account & login are both set to true
   -uses jquery to toggle between the forms


v.1.2.5
=======
Fixed missing configuration values from install routine


v.1.2.4
=======
Fix reCAPTCHA for login page


v1.2.3
======
Updated source for reCATPCHA servers moving to Google


v1.2.2
======
Added links to reCAPTCHA signup page when installed
Fixed HTTPS/SSL support. Script was not properly detecting secure pages


v1.2.1
======
Links are not displayed when module is not installed, clicking link will install configuration values
Link displayed on configuration page to uninstall module


v1.2
====
Fixed folder for template files
Added reCAPTCHA for pages advanced search, create account, login, product write review, tell a friend
Used template files from 1.3.9g


v1.1
====
Fixed some query syntax
Update native mysql queries to use $db:Factory
Fixed install/uninstall


v1.0
====
Intial release


-------------------------
Step by step instructions
-------------------------

** Original files located in /includes/modules/pages/(page name)/header.php **
** !!IMPORTANT!! Backup these files **
 
1. connect to the URL https://www.google.com/recaptcha/admin/create and create a reCaptcha account;
2. add your web domain to your new reCaptcha account, according to instructions;
3. in this package rename \includes\templates\CUSTOM to your template directory.
4. upload all the files to your store.
5. login to your admin page in zen cart
6. Click link in at top of page labeled [Install Now]. recaptcha configuration will itself & sql statemtents. 
7. goto Configuration->reCAPTCHA or click on link at top of page [Click to Configure].
8. edit the values in the configuration. You will need your public & private key given from the
	reCAPTCHA website, and choose your theme.
9. (optional) edit file /includes/languages/english/extra_definitions/recaptcha_definitions.php and change any of the constants to suit you.

That's all!

----------------------
Upgrading Instructions
----------------------
1. Reupload package files
2. Login to admin page
3. Goto Configuration->reCAPTCHA.
4. Click on link to remove/upgrade
5. Click on link to install


Files in this package

** New Files **

	\admin\includes\functions\extra_functions\recaptcha_functions.php
	\admin\includes\extra_datafiles\recaptcha_filenames.php
	\includes\classes\recaptchalib.php
	\includes\languages\english\extra_definitions\contact_us_definitions.php
	\includes\templates\CUSTOM\tpl_(page_name)_default.php (added 1 block of code to default file)

** Modified core files **

	\includes\modules\pages\(page_name)\header_php.php

